import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import firebase from 'firebase';
@Injectable()
export class CheckbookAccountsProvider {

  public userProfileRef:firebase.database.Reference;
  public dates:firebase.database.Reference;
  constructor() {
    firebase.auth().onAuthStateChanged( user => {
      if (user) {
        this.userProfileRef = firebase.database().ref(`userProfile/${user.uid}`); 
      }
    });
  }
  getAccountList(): firebase.database.Reference {

    return this.userProfileRef;
  }

delete(accountname)
{
this.userProfileRef.ref.child(`${accountname}`).remove();
}
  getEventDetail(accountname:string): firebase.database.Reference {
    return this.userProfileRef.child(`${accountname}`);
  }
  

 
 createAccount( date: Date,accountname: string, paybudget: number, opbalance: number): firebase.Promise<any> {
      
    return this.userProfileRef.child(accountname).set({
      date: new Date().toString(),
      name: accountname,
      paybudget: paybudget,
      opbalance: opbalance
    });
 }
 
  createAccounts( date: Date,accountname: string, paybudget: number, opbalance: number): firebase.Promise<any> {
      
    return this.userProfileRef.child(accountname).child('Journal').child(date.toString()).set({
    	date: new Date(date).toString(),
    num: '',
    details:'Opening Balance', 
    withdrawal:'0',
    deposit:opbalance,
    bank:'',
    balance:opbalance
 })
  }
 
  
  addJournal(date: Date, num: string, details:string,withdrawal: number,deposit:number,accountId,bank:string,balance:number):firebase.Promise<any> {
  // var storesRef=firebase.database.ref().

  //let account=accountname;
    return this.userProfileRef.child(accountId).child('Journal ').child(date.toString()).set({
    	date: new Date(date).toString(),
    num: num,
    details:details, 
    withdrawal:withdrawal,
    deposit:deposit,
    bank:bank,
    balance:deposit-withdrawal
  
  });

}


addentry(accountId:string,date:Date,num: string,details:string,withdrawal: number,deposit:number,bank:string,balance:number):firebase.Promise<any> {
     return this.userProfileRef.child(accountId).child('Journal').child(date.toString()).set({
    	date: new Date(date).toString(),
    num: num,
    details:details, 
    withdrawal:withdrawal,
    deposit:deposit,
    bank:bank, 
    balance:deposit-withdrawal 
  });

}
adddeposit(date:Date,details:string,deposit:number,accountId):firebase.Promise<any>
{
  return this.userProfileRef.child(accountId).child('Journal').child(date.toString()).set({
    	date: new Date(date).toString(),
    num: 'Deposits',
    details:details, 
    withdrawal:'0',
    deposit:deposit,
    bank:'', 
    balance:deposit
  });
}

 subtransferdetails(date: Date, amount: number,accountId:string, to:string,reason:string):firebase.Promise<any> {
  // var storesRef=firebase.database.ref().

  //let account=accountname;
    return this.userProfileRef.child(accountId).child('Journal').child(date.toString()).set({
    	date: new Date(date).toString(),
    num: 'NEFT',
    details:reason, 
    withdrawal:amount,
    deposit:'0',
    bank:'', 
    balance:0-amount 
  });

 }
addtransferdetails(date: Date, amount: number,accountId:string, to:string,reason:string):firebase.Promise<any> {
  // var storesRef=firebase.database.ref().

  //let account=accountname;
    return this.userProfileRef.child(to).child('Journal').child(date.toString()).set({
    	date: new Date(date).toString(),
    num: 'NEFT',
    details:reason, 
    withdrawal:'0',
    deposit:amount,
    bank:'', 
    balance:amount
  
  });

 }

  getdates():firebase.database.Reference{
    return this.dates;
  }
}

