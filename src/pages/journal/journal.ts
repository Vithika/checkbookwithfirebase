import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams ,ToastController} from 'ionic-angular';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import firebase from 'firebase';
import { CheckbookAccountsProvider } from '../../providers/checkbook-accounts/checkbook-accounts';
import { Validators,FormBuilder,FormGroup }from'@angular/forms';
/**
 * Generated class for the JournalPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-journal',
  templateUrl: 'journal.html',
})
export class JournalPage {
accountList:Array<any>;
  journal:FormGroup;
   public currentEvent: any = {};
   date:Date;
//public journal:firebase.database.Reference;
  constructor(public navCtrl: NavController,public navParams:NavParams, public  CheckbookAccountsProvider:CheckbookAccountsProvider, public formBuilder:FormBuilder,public params: NavParams,public af: AngularFireDatabase,public toastCtrl:ToastController) {

       this.journal = this.formBuilder.group({
			date: ['', Validators.required],
			withdrawal: [''],
			deposit:[''],
			bank:[''],
			num:[''],
			details:['',Validators.required]
		});
  
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad JournalPage');
  }

  /*ionViewDidEnter(){
    this.CheckbookAccountsProvider.getEventDetail(this.navParams.get('eventId'))
    .on('value', eventSnapshot => {
      this.currentEvent = eventSnapshot.val();
      this.currentEvent.id= eventSnapshot.key;
    });
  }*/
/*ionViewWillEnter() {
		  this.CheckbookAccountsProvider.getAccountList().on('value', snapshot => {
      this.accountList = [];
      snapshot.forEach( snap => {
        this.accountList.push({
          id: snap.key,
          name: snap.val().name
        });      
        return false
      });
    });
		}*/
	
/*journaldata(date:Date,num:string,details:string,withdrawal:number,deposit:number,bank:string)
  {
    
    this.CheckbookAccountsProvider.addJournal(date,num, details, withdrawal,deposit,name, bank).then(newaccount=>
    {
      this.navCtrl.pop();
       console.log("journal="+date,num,details,withdrawal,deposit,bank);
      this._presentToast("Transactions added successfully");
    },error=>
    {
      console.log(error);
    
    })
   
    
  }
 
 _presentToast(msg) {
		let toast = this.toastCtrl.create({
		message: msg,
		duration: 3000,
		position: 'bottom'
		});

		toast.onDidDismiss(() => {
			// console.log('Dismissed toast');
		});

		toast.present();
	}
 

}*/
}
