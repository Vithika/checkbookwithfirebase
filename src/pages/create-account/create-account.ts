import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController } from 'ionic-angular';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import firebase from 'firebase';
import { CheckbookAccountsProvider } from '../../providers/checkbook-accounts/checkbook-accounts';
/**
 * Generated class for the CreateAccountPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-create-account',
  templateUrl: 'create-account.html',
})

export class CreateAccountPage {
date:Date;
accountList: firebase.database.Reference;
  constructor(public navCtrl: NavController, public navParams: NavParams,
  public CheckbookAccountsProvider:CheckbookAccountsProvider, public af: AngularFireDatabase,public toastCtrl:ToastController) {
 this.date = new Date(); 
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreateAccountPage');
  }

   createAccount( date :Date,accountname :string, payperiodbudget :number, opbalance:number) {

  this.CheckbookAccountsProvider.createAccount(date,accountname,payperiodbudget,opbalance).then( newaccount => {
      
      this._presentToast("Successfully created account: "+name +new Date().toLocaleString());
    }, error => {
      console.log(error);
    });

     this.CheckbookAccountsProvider.createAccounts(date,accountname,payperiodbudget,opbalance).then( newaccount => {
      
     // this._presentToast("Successfully created account: "+name +new Date().toLocaleString());
    }, error => {
      console.log(error);
    });

}

   
 
_presentToast(msg) {
		let toast = this.toastCtrl.create({
		message: msg,
		duration: 3000,
		position: 'bottom'
		});

		toast.onDidDismiss(() => {
			// console.log('Dismissed toast');
		});

		toast.present();
	}
 


}