import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController } from 'ionic-angular';

import { CheckbookAccountsProvider } from '../../providers/checkbook-accounts/checkbook-accounts';
/**
 * Generated class for the BudgetdepositPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-budgetdeposit',
  templateUrl: 'budgetdeposit.html',
})
export class BudgetdepositPage {
accountList:Array<any>;
currentEvent:any;
date:Date;
  constructor(public navCtrl: NavController, public navParams: NavParams,public CheckbookAccountsProvider:CheckbookAccountsProvider,public toastCtrl:ToastController) {
  this.date = new Date(); 
 }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BudgetdepositPage');
  }
  ionViewDidEnter(){
    this.CheckbookAccountsProvider.getEventDetail(this.navParams.get('eventId'))
    .on('value', eventSnapshot => {
      this.currentEvent = eventSnapshot.val();
      this.currentEvent.id= eventSnapshot.key;
    });
    console.log(this.currentEvent.id);
  }
depositbudget(date:Date,details:string,deposit:number)
  {
    
    this.CheckbookAccountsProvider.adddeposit(date,details,deposit,this.currentEvent.id).then(newaccount=>
    {
      this.navCtrl.pop();
       console.log("deposit="+date,details,deposit);
      this._presentToast("Deposits added successfully") ;
       },error=>
    {
      console.log(error);
    
    })
   
    
  }
 
 _presentToast(msg) {
		let toast = this.toastCtrl.create({
		message: msg,
		duration: 3000,
		position: 'bottom'
		});

		toast.onDidDismiss(() => {
			// console.log('Dismissed toast');
		});

		toast.present();
	}
 
	
}
