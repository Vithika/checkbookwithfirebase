import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController} from 'ionic-angular';

import { CheckbookAccountsProvider } from '../../providers/checkbook-accounts/checkbook-accounts';
/**
 * Generated class for the TransferPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-transfer',
  templateUrl: 'transfer.html',
})
export class TransferPage {
accountList:Array<any>;
date:Date;
  constructor(public navCtrl: NavController, public toastCtrl:ToastController,
   public navParams: NavParams,public CheckbookAccountsProvider:CheckbookAccountsProvider) {
this.date = new Date();   
}

  ionViewDidLoad() {
    console.log('ionViewDidLoad TransferPage');
  }

  ionViewWillEnter() {
		  this.CheckbookAccountsProvider.getAccountList().on('value', snapshot => {
      this.accountList = [];
      snapshot.forEach( snap => {
        this.accountList.push({
          id: snap.key,
          name: snap.val().name
        });      
        return false
      });
    });
		}

transferdata(date:Date,amount:number,from:string, to:string,reason :string)
{
  if(from==to)
  {
    this.presentToast("Cannot transfer within the same accounts");
      return;
  }
  else
  {
 this.CheckbookAccountsProvider.addtransferdetails(date,amount,from,to,reason).then( newaccount => {
      
      this.presentToast("Successfully Transferred amount: "+name +new Date().toLocaleString());
    }, error => {
      console.log(error);
    });
this.CheckbookAccountsProvider.subtransferdetails(date,amount,from,to,reason).then( newaccount => {
     
     // this.presentToast("Successfully Transferred amount: "+name +new Date().toLocaleString());
    }, error => {
      console.log(error);
    });

}
}



presentToast(msg) {
		let toast = this.toastCtrl.create({
		message: msg,
		duration: 3000,
		position: 'bottom'
		});

		toast.onDidDismiss(() => {
			// console.log('Dismissed toast');
		});

		toast.present();
	}

}
