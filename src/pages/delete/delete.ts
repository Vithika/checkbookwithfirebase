import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController,AlertController} from 'ionic-angular';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import firebase from 'firebase';
import { CheckbookAccountsProvider } from '../../providers/checkbook-accounts/checkbook-accounts';
import { Validators,FormBuilder,FormGroup }from'@angular/forms';
import{BudgetdepositPage}from'../budgetdeposit/budgetdeposit';
import{SummaryPage}from'../summary/summary';
import{TransferPage}from'../transfer/transfer';
import {HomePage}from'../home/home';
/**
 * Generated class for the DeletePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-delete',
  templateUrl: 'delete.html',
})
export class DeletePage {

   public accountList: Array<any>;
public userProfileRef:firebase.database.Reference;
account:any;
  constructor(public navCtrl: NavController, public toastCtrl:ToastController, public alertCtrl:AlertController,  public navParams: NavParams,public af:AngularFireDatabase,public CheckbookAccountsProvider:CheckbookAccountsProvider) {
  }
   ionViewDidEnter() {
     
    this.CheckbookAccountsProvider.getAccountList().on('value', snapshot => {
      this.accountList = [];
      snapshot.forEach( snap => {
        this.accountList.push({
          id: snap.key,
          name: snap.val().name
        
        });
          
        return false
      });
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DeletePage');
  }

  del(id)
  {
     this.presentConfirm(id);
  }
  delaccount(id)
  {
    this.CheckbookAccountsProvider.delete(id);
  //  firebase.database().ref(this.userProfileRef);

}
/*transfer(id)
{
  this.navCtrl.push(TransferPage);
}*/

presentConfirm(id) {
  let alert = this.alertCtrl.create({
    title: 'Delete Account',
    message: 'Do you want to  delete this Account?',
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Delete',
        handler: () => {
          this.delaccount(id);
        }
      }
    ]
  });
  alert.present();
}
add(eventId)
{
  this.navCtrl.push(JournalPage,
  {eventId:eventId});
  this._presentToast("Fill the details to complete your Transactions");
      
 //  this.navCtrl.push('event-detail', { 'eventId': eventId });
}

 _presentToast(msg) {
		let toast = this.toastCtrl.create({
		message: msg,
		duration: 3000,
		position: 'bottom'
		});

		toast.onDidDismiss(() => {
			// console.log('Dismissed toast');
		});

		toast.present();
	}
 
deposit(eventId)
{
  this.navCtrl.push(BudgetdepositPage,
  {
        eventId:eventId });

       this._presentToast("Fill the details to Add deposits");
         
}

}
@Component({
  template:`
  
  <ion-header>

  <ion-navbar>
    <ion-title>JOURNAL</ion-title>
  </ion-navbar>

</ion-header>


<ion-content class="background">


        <ion-item>
          <ion-label>Date<span>&nbsp;<sup>*</sup></span></ion-label>
          <ion-input [(ngModel)]="date" type="text" placeholder="enter the date"></ion-input>
        </ion-item>

         <ion-item>
          <ion-label>Num<span>&nbsp;<sup>*</sup></span></ion-label>
          <ion-input [(ngModel)]="num" type="text" placeholder="Fill num "></ion-input>
        </ion-item>
        

        <ion-item>
          <ion-label>Details<span>&nbsp;<sup>*</sup></span></ion-label>
          <ion-textarea [(ngModel)]="details" type="text" placeholder="Fill the details"></ion-textarea>
        </ion-item>
        
        <ion-item>
          <ion-label>Withdrawal<span>&nbsp;<sup>*</sup></span></ion-label>
          <ion-input [(ngModel)]="withdrawal" type="text" placeholder="Fill your withdrawal amount"></ion-input>
        </ion-item>
        
        <ion-item>
          <ion-label>Deposit<span>&nbsp;<sup>*</sup></span></ion-label>
          <ion-input [(ngModel)]="deposit" type="text" placeholder="Fill your deposit amount"></ion-input>
        </ion-item>
        
       <!-- <ion-item>
          <ion-select [(ngModel)]="name" multiple="false">
	    <ion-option  *ngFor="let account of accountList"[value]="account.name">{{account.name}}</ion-option>
	</ion-select>
        </ion-item>-->
        
        <ion-item>
          <ion-label>Bank Reconcilation<span>&nbsp;<sup>*</sup></span></ion-label>
          <ion-input [(ngModel)]="bank" type="text" ></ion-input>
        </ion-item>

        <br>
          <button ion-button type="submit" (click)="journaldata(date,num,details,withdrawal,deposit,bank)" full>Add Entry</button>
</ion-content>
`

})

export class JournalPage {
accountList:Array<any>;
  journal:FormGroup;
date:Date;
balance:any;
   public currentEvent: any = {};
//public journal:firebase.database.Reference;
  constructor(public navCtrl: NavController,public navParams:NavParams, public  CheckbookAccountsProvider:CheckbookAccountsProvider, public formBuilder:FormBuilder,public params: NavParams,public af: AngularFireDatabase,public toastCtrl:ToastController) {

       this.journal = this.formBuilder.group({
			date: ['', Validators.required],
			withdrawal: [''],
			deposit:[''],
			bank:[''],
			num:[''],
			details:['',Validators.required]
		});
    this.date = new Date();

  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad JournalPage');
  }

  ionViewDidEnter(){
    this.CheckbookAccountsProvider.getEventDetail(this.navParams.get('eventId'))
    .on('value', eventSnapshot => {
      this.currentEvent = eventSnapshot.val();
      this.currentEvent.id= eventSnapshot.key;
    });
  }

	
journaldata(date,num:string,details:string,withdrawal:number, deposit:number,bank:string)
  {
    
    this.CheckbookAccountsProvider.addJournal(date,num, details, withdrawal,deposit,this.currentEvent.id,bank,this.balance).then(newaccount=>
    
	
    {
      
      this.navCtrl.pop();
       console.log("journal="+date,num,details,withdrawal,deposit,bank,this.balance);
      this._presentToast("Transactions added successfully");
    
    },error=>
    {
      console.log(error);
    
    })
    
    
  }
  
 
 _presentToast(msg) {
		let toast = this.toastCtrl.create({
		message: msg,
		duration: 3000,
		position: 'bottom'
		});

		toast.onDidDismiss(() => {
			// console.log('Dismissed toast');
		});

		toast.present();
	}
 

}


