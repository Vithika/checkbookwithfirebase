import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { CreateAccountPage } from '../create-account/create-account';
import{DeletePage}from'../delete/delete';
import{JournalPage}from'../journal/journal';
import{UserguidePage}from'../userguide/userguide';
import{TransferPage}from'../transfer/transfer';
import{SummaryPage}from'../summary/summary';
import{BudgetdepositPage}from'../budgetdeposit/budgetdeposit';
import { AuthProvider } from '../../providers/auth/auth';
import{LoginPage}from'../login/login';
@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {

  constructor(public navCtrl: NavController,public authProvider:AuthProvider) {

  }

addaccount(){

    this.navCtrl.push(CreateAccountPage);

  }
  delete()
  {
    this.navCtrl.push(DeletePage);
  }
  update()
  {
    this.navCtrl.push(DeletePage);
  }
  loadjournal()
  {
    this.navCtrl.push(JournalPage);
  }
  loaduserguide()
  {
    this.navCtrl.push(UserguidePage);
  }
  viewsummary()
  {
this.navCtrl.push(SummaryPage);
  }
  loadtransfer()
  {
this.navCtrl.push(TransferPage);
  }
  loadbudget()
  {
this.navCtrl.push(BudgetdepositPage);
  }

  	logOut(): void {
    this.authProvider.logoutUser().then(() => {
      this.navCtrl.setRoot(LoginPage);
    });
  }
}
