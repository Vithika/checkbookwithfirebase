import { Component } from '@angular/core';
import { NavController,NavParams,ToastController} from 'ionic-angular';
import{FirebaseListObservable}from'angularfire2/database';
import{AngularFireDatabase}from'angularfire2/database';
import{AngularFireModule}from'angularfire2';
import { CreateAccountPage } from '../create-account/create-account';
import { AuthProvider } from '../../providers/auth/auth';
import{LoginPage}from'../login/login';
import firebase from 'firebase';
import { CheckbookAccountsProvider } from '../../providers/checkbook-accounts/checkbook-accounts';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

public from:any;
public deposit:any;
public withdrawal:any;
public num:any;
public details:any;

  // public accountList: firebase.database.Reference;
 public accountList: Array<any>;
 public checkbook:Array<any>;
display: any = {};
currentEvent:any;
entry: any = {};
date:Date;
balance:any;
bank:any;
public userProfileRef:firebase.database.Reference;
public dates:firebase.database.Reference;
public datesList:Array<any>;
public transferList:Array<any>;
public totalJournalbalance:any=0;
public totalbalance:any=0;
public totaldepositbalance:any=0;
public amounttransferred:any=0;
public amountreceived:any=0;


  constructor(public navCtrl: NavController,
	public toastCtrl:ToastController,
  public navParams:NavParams,
	public af: AngularFireDatabase,
	public authProvider:AuthProvider,
	public CheckbookAccountsProvider:CheckbookAccountsProvider) {
		 firebase.auth().onAuthStateChanged( user => {
      if (user) {
        this.userProfileRef = firebase.database().ref(`userProfile/${user.uid}`); 
      }
    });
  
this.date=new Date();

this.display = { help: true, sample: false, checkbook: false };

//this.getAccountList();
    


  }
	 getAccountLists():firebase.database.Reference {

    return this.userProfileRef;
  }
/*	getAccountList(): firebase.database.Reference {
    return this.accountList.child('/accounts');
  }
*/
 ionViewDidEnter() {
		  this.getAccountLists().on('value', snapshot => {
      this.accountList = [];
      snapshot.forEach( snap => {
        this.accountList.push({
          id: snap.key,
          name: snap.val().name
        });      
        return false
      });
    });
		}

   
 

 
 
/* ionViewDidEnter(){
    this.CheckbookAccountsProvider.getAccountcheckbook(this.navParams.get('eventId'),)
    .on('value', eventSnapshot => {
      this.currentEvent = eventSnapshot.val();
      this.currentEvent.id= eventSnapshot.key;
    });
  }
*/


 open(field){
		switch (field) {
			case "help":
				this.display.checkbook = false;
				this.display.help = true;
				this.display.sample = false;
			break;
			case "sample":
				this.display.checkbook = false;
				this.display.help = false;
				this.display.sample = true;
			break;
			case "checkbook":
				this.display.checkbook = true;
				this.display.help = false;
				this.display.sample = false;
			break;
			
			default:
				// code...
				break;
		}
		// console.log("State change => "+JSON.stringify(this.display));
	}
	clear()
	{
this.from=null;
this.deposit=null;
this.withdrawal=null;
this.num=null;
this.details=null;
this.bank=null;
	}
  logEntry(from:string,date:Date,num:string,details:string,withdrawal:number,deposit:number,bank:string)
  {
    
	
	
    this.CheckbookAccountsProvider.addentry(from,date,num,details,withdrawal,deposit,bank,this.balance).then(newentry=>
    {
      //this.navCtrl.pop();
       console.log("journal="+date,num,details,withdrawal,deposit,from,bank);
      this._presentToast("Entries added successfully");
    },error=>
    {
      console.log(error);
    })
  }
	
   
    _presentToast(msg) {
		let toast = this.toastCtrl.create({
		message: msg,
		duration: 3000,
		position: 'bottom'
	});

		toast.onDidDismiss(() => {
			// console.log('Dismissed toast');
		});

		toast.present();
	}

	 getdates():firebase.database.Reference{
    return this.dates;
  }

  updateSelectedAccount()
	{
		this.transferList=null;
		this.datesList=null;
		this.totalbalance=0;
		this.totaldepositbalance=0;
		this.totalJournalbalance=0;
		this.amountreceived=0;
		this.amounttransferred=0;
	}

	showsjournal(accountname)
  {
console.log(accountname.name);
    this.dates=this.userProfileRef.child(accountname.name).child('Journal').child('/');
    console.log(this.dates);
    console.log(name);
    this.totalJournalbalance=0;
 this.getdates().on('value',snapshot=>
 {
   this.datesList=[];
   snapshot.forEach(snap=>
   {
     this.datesList.push({
       date:snap.val().date,
       withdrawal:snap.val().withdrawal,
       deposit:snap.val().deposit,
       num:snap.val().num,
       details:snap.val().details,
       balance:snap.val().balance
      
     });
      this.totalJournalbalance+=parseInt(snap.val().balance);
       console.log(this.totalJournalbalance);
       this.totalbalance=parseInt(this.totaldepositbalance)+parseInt(this.totalJournalbalance)-parseInt(this.amounttransferred);
       
    
     return false
     
   });
   
   console.log(this.datesList);
}
  
 )}
  

	


     
}
