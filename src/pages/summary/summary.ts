import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CheckbookAccountsProvider } from '../../providers/checkbook-accounts/checkbook-accounts';
/**
 * Generated class for the SummaryPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-summary',
  templateUrl: 'summary.html',
})
export class SummaryPage {

  constructor(public navCtrl: NavController, public CheckbookAccountsProvider:CheckbookAccountsProvider, public navParams: NavParams) {
  }

 ionViewWillEnter() {
		 
     
		}

  

  /*ionViewWillEnter(){
    this.CheckbookAccountsProvider.getEventDetail(this.navParams.get('eventId'))
    .on('value', eventSnapshot => {
      this.currentEvent = eventSnapshot.val();
      this.currentEvent.id= eventSnapshot.key;
    });
    console.log(this.currentEvent.id);
    this.ionViewDidEnter();
  }*/
  

  ionViewDidLoad() {
    console.log('ionViewDidLoad SummaryPage');
  }

}