import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';
import{LoginPage}from'../pages/login/login';
import firebase from 'firebase';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
 rootPage:any;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {



       firebase.initializeApp({
    apiKey: "AIzaSyACkjCWlmFjmIwGwJaS960cRGqB0yRjKSA",
    authDomain: "authorisation-fc568.firebaseapp.com",
    databaseURL: "https://authorisation-fc568.firebaseio.com",
    projectId: "authorisation-fc568",
    storageBucket: "authorisation-fc568.appspot.com",
    messagingSenderId: "396688252314"
  
  
    });

   

    const unsubscribe = firebase.auth().onAuthStateChanged((user) => {
      if (!user) {
        this.rootPage = LoginPage;
        unsubscribe();
      } else { 
        this.rootPage = TabsPage;
        unsubscribe();
      }
    });

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}

  
   /* firebase.initializeApp({
    apiKey: "AIzaSyACkjCWlmFjmIwGwJaS960cRGqB0yRjKSA",
    authDomain: "authorisation-fc568.firebaseapp.com",
    databaseURL: "https://authorisation-fc568.firebaseio.com",
    projectId: "authorisation-fc568",
    storageBucket: "authorisation-fc568.appspot.com",
    messagingSenderId: "396688252314"
  
    });*/


    /* const unsubscribe = firebase.auth().onAuthStateChanged((user) => {
      if (!user) {
        this.rootPage = LoginPage;
        unsubscribe();
      } else { 
        this.rootPage = TabsPage;
        unsubscribe();
      }
    });*/

    


