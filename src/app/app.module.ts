import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
//import{JournalPage}from'../pages/journal/journal';
import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { AngularFireDatabaseModule, AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import{AngularFireModule} from'angularfire2';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import{CreateAccountPage}from'../pages/create-account/create-account';
import{DeletePage,JournalPage}from'../pages/delete/delete';
import{UserguidePage}from'../pages/userguide/userguide';
import{TransferPage}from'../pages/transfer/transfer';
import{SummaryPage}from'../pages/summary/summary';
import{BudgetdepositPage}from'../pages/budgetdeposit/budgetdeposit';
import{LoginPage}from'../pages/login/login';
import{SignupPage}from'../pages/signup/signup';
import { AuthProvider } from '../providers/auth/auth';
import firebase from 'firebase';
import { CheckbookAccountsProvider } from '../providers/checkbook-accounts/checkbook-accounts';


 export const firebaseConfig ={
    apiKey: "AIzaSyACkjCWlmFjmIwGwJaS960cRGqB0yRjKSA",
    authDomain: "authorisation-fc568.firebaseapp.com",
    databaseURL: "https://authorisation-fc568.firebaseio.com",
    projectId: "authorisation-fc568",
    storageBucket: "authorisation-fc568.appspot.com",
    messagingSenderId: "396688252314"
  
  };
  
@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    CreateAccountPage,
    DeletePage,
    JournalPage,
    BudgetdepositPage,
    TransferPage,SummaryPage,LoginPage,
    UserguidePage,SignupPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig),
   
     
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    CreateAccountPage,
    DeletePage,
    JournalPage,
    BudgetdepositPage,TransferPage,SummaryPage,
    UserguidePage,
    LoginPage,
    SignupPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AngularFireDatabase,
    AngularFireModule,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthProvider,
    CheckbookAccountsProvider
  ]
})
export class AppModule {}
